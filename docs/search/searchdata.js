var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstwxy",
  1: "ns",
  2: "lmnps",
  3: "abcdefghinprsw",
  4: "bcdgilmorstwxy",
  5: "e",
  6: "l",
  7: "l",
  8: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules",
  8: "Pages"
};

